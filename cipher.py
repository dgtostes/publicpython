from selenium import webdriver
import datetime

for i in range(0,100): 
    # Firefox driver
    driver = webdriver.Firefox()
     
    # Go to cipher website
    driver.get('http://www.cipher.com.br/companhia/trabalhe-conosco')
     
    #insert data
    first_name = driver.find_element_by_id('edit-submitted-first-name')
    first_name.send_keys("diego")

    first_name = driver.find_element_by_id('edit-submitted-last-name')
    first_name.send_keys("tostes")                                     

    email = driver.find_element_by_id('edit-submitted-email')
    email.send_keys("dtostes@gmail.com")

    confirm_email = driver.find_element_by_id('edit-submitted-confirm-email')
    confirm_email.send_keys("dtostes@gmail.com")

    now = datetime.datetime.now()
    telefone = driver.find_element_by_id('edit-submitted-phone')
    telefone.send_keys("quebrando captcha %s" % now.ctime())

    soma = driver.find_element_by_class_name("field-prefix")
    resultado = eval((str(soma.text)).strip(" ="))

    result_field = driver.find_element_by_id('edit-captcha-response')
    result_field.send_keys(str(resultado))
     
    # Submit the form!
    submit_button = driver.find_element_by_id('edit-submit')
    submit_button.click()


    # Close the browser!
    driver.quit()