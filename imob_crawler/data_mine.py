from BeautifulSoup import *
import sys
import os
import datetime
import math
import urllib2

 
def generate_info_dic(url):
    dic = {}    
    texto = urllib2.urlopen(url)
    texto2 = texto  
                  
    texto = urllib2.urlopen(url)
    soup = BeautifulStoneSoup(texto)
    #soup('title')[0].string
    infos = soup('ul')
    
    
    for i in infos:
        for li in i.findAll('li'):
            for a in li.findAll('span'):
                try:
                    id = a['id']
                    #print id
                    #print a.string
                    if id == "ctl00_ContentPlaceHolder1_detalhes_lbDorms":
                        dic['quartos'] =  int(str(a.string).split()[0])
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbAndar":
                        dic['andares'] = a.string 
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbConstrucao":
                        dic['ano_constr'] = int(a.string) 
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbPrecoVenda":
                        dic['preco'] = float(str(a.string).replace(".","").split()[1])
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbCondominio":
                        dic['condominio'] = float(str(a.string).replace(".","").split()[1]) 
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbIPTU":
                        dic['iptu'] =  float(str(a.string).replace(".","").split()[1])
                    elif id == "ctl00_ContentPlaceHolder1_detalhes_lbValorM2":
                        dic['valor_m2'] = float(str(a.string).replace(".","").split()[1])                                       
                    try:
                        dic['area'] = dic['preco']/dic['valor_m2']
                    except:
                        dic['area'] = None
                        
                    dic['crawler_date'] = datetime.date.today()
                except:
                    pass
                    
                    dic['url'] = url
                    
    
    texto = urllib2.urlopen(url)              
    for i in texto:
        if "localidadecidade" in i:
            try:
                i = i.replace("'","").replace(",","")
                dic['cidade'] = i.split(":")[1].strip("\r\n").strip(" ")
            except:
                dic['cidade'] = None
        elif "localidadelocalidade:" in i:
            try:
                i = i.replace("'","").replace(",","")
                dic['zona'] = i.split(":")[1].strip("\r\n").strip(" ")
            except:
                dic['zona'] = None
        elif "localidadebairro" in i:
            try:
                i = i.replace("'","").replace(",","")
                dic['bairro'] = i.split(":")[1].strip("\r\n").strip(" ")
            except:
                dic['bairro'] = None

    return dic



if __name__ == "__main__":
    url = "http://www.zap.com.br/imoveis/oferta/Apartamento-Padrao-1-quartos-venda-RIO-DE-JANEIRO-BOTAFOGO-PRAIA-BOTAFOGO-DE/ID-2284592"
    print generate_info_dic(url)


