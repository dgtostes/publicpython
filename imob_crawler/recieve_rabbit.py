#!/usr/bin/env python
import pika
import sqlite3

def insert_link(db, link):
    if is_link_in_db(link):
        pass
    else:
        con = sqlite3.connect('crawler.db')
        cur = con.cursor()
        sql = 'insert into links values (null, "%s", 0, 0)' % link
        cur.execute(sql)
        con.commit()
        con.close()
    
def is_link_in_db(link):
    con = sqlite3.connect('crawler.db')
    cur = con.cursor()
    sql = 'select count(id) from links where link = "%s";'  % link
    cur.execute(sql)
    recset = cur.fetchall()
    if recset[0][0] == 0:
        return False
    else:
        return True


if __name__ == "__main__":
    
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='crawler')

print ' [*] Waiting for messages. To exit press CTRL+C'

def callback(ch, method, properties, body):
    
    print " [x] Received %r" % (body,)
    try:
        insert_link('crawler.db', body)
    except:
        pass

channel.basic_consume(callback,
                      queue='crawler',
                      no_ack=True)

channel.start_consuming()
    