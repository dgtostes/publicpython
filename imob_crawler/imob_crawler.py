# -*- coding: utf-8 -*-
import urllib2
from BeautifulSoup import BeautifulSoup
import pika
import sqlite3

class Crawler(object):
    def __init__(self, url, queue):
        self.url = url
        self.queue = queue
        
    
    def send_links(self):
        request = urllib2.Request(self.url)
        response = urllib2.urlopen(request)
        document = response.read()
        #normaliza o documento para que o mesmo seja acessível via objetos
        soup = BeautifulSoup(document) 
        
        # retorna uma lista com todos os links do documento
        links = soup.findAll('a')  
        
        for link in links:
            try:
                ver = link['href']
            except:
                ver = None
            if ver:
                if is_offer(link['href']):#"http://www.zap.com.br" in link['href'] and "ID-" in link['href']:
                    try:
                        self.__send2queue(link['href'])
                    except:
                        print "not sended"
                                
    
    def __send2queue(self, url2send):
        connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
        channel = connection.channel()
        
        channel.queue_declare(queue=self.queue)
        
        channel.basic_publish(exchange='',
                              routing_key=self.queue,
                              body=url2send)
        print url2send
        connection.close()
                    
def gera_lista_consulta(db):
    con = sqlite3.connect('crawler.db')
    cur = con.cursor()
    sql = 'select link from links where ver = 0'
    cur.execute(sql)
    recset = cur.fetchall()
    links_list = []
    for rec in recset:
        links_list.append(rec[0])
    return links_list

def mark_link_as_checked(link, db):
        con = sqlite3.connect(db)
        cur = con.cursor()
        sql = 'update links set ver=1 where link = "%s"' % link
        cur.execute(sql)
        con.commit()
        con.close()    

def start_app(db, queue):
    while True:
        link_list = gera_lista_consulta(db)
        for l in link_list:
            Crawler(l, queue).send_links()
            try:
                mark_link_as_checked(l, db)
            except:
                "not marked"
            
def is_offer(link):
    not_list = ["twitter", 'facebook', 'cadastro']
    yep_list = ["www.zap.com.br", "ID-"]
    for i in not_list:
        if i in link:
            return False
    count = 0
    for i in yep_list:
        if i not in link:
            return False
    if count == 0:
        return True

        
            
if __name__ == "__main__":
    db = 'crawler.db'
    queue = "crawler"
    start_app(db, queue)
