import requests

headers = {
  'User-Agent': 'Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1'
  }
  
biscoitos = {'cade-meu-cookie':"'esta aqui'",'d53db4de415c4e858dc761595623a898':'+', '18':'+'}
'''
cade-meu-cookie --> level 4
d53db4de415c4e858dc761595623a898 --> level 2
18 --> level 31

como descobri?
  req = requests.get(url, headers=headers, cookies=None)
  print req.headers
  >> exibindo cookies e dicas

'''

def trataTexto(texto):
  bonsChars = [str(i) for i in range(0,10)] + ['.', ',', '$']
  texto = texto.replace(" ", "")
  if '$' not in texto:
    return None
  else:
    indexCifra = texto.index("$")
  textoParaChecar = texto[indexCifra:-1]

  for letra in textoParaChecar:
    if letra not in bonsChars:
      indexFinal = textoParaChecar.index(letra)
      break
    else:
      indexFinal = -1

  preco = textoParaChecar[1:indexFinal]
  if ("." in preco) and ("," in preco):
    preco = preco.replace(".", "").replace(",", ".")

  elif "," in preco:
    preco = preco.replace(",", ".")
  
  return float(preco)

def pegapagina(url):
  req = requests.get(url, headers=headers, cookies=biscoitos)
  response_text = req.text
  return response_text


if __name__ == "__main__":
  url_root = "http://wonder.sieve.com.br:9090/level%s"

  for i in range(1,6):
    pagina = pegapagina(url_root % i)
    preco = trataTexto(pagina)
    print "level: %s -- preco: %s pagina: %s" % (i,preco, pagina)
