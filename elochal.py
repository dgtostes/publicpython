#autor: diego tostes
#referencias: http://www.tutorialspoint.com/python/python_multithreading.htm
#modo de usar: #python elochal.py "termo de busca"


from pygoogle import pygoogle
import urllib2
from BeautifulSoup import BeautifulSoup
import time
import sys
from itertools import count, combinations
import logging
import random
import threading
import time
import Levenshtein
import Queue

class Counter(object):
    def __init__(self, start=0):
        self.lock = threading.Lock()
        self.value = start
    def increment(self):
        logging.debug('Waiting for lock')
        self.lock.acquire()
        try:
            logging.debug('Acquired lock')
            self.value = self.value + 1
        finally:
            self.lock.release()


class myThread (threading.Thread):
    def __init__(self, url, search_tolken, c):
        threading.Thread.__init__(self)
        self.url = url
        self.search_tolken = search_tolken 
        self.c = c
    def run(self):
        print "Starting " + self.url
        get_title_thread(self.url, self.search_tolken, self.c)
        print "Exiting " + self.url




def check_tolken(tolken, title):
	tolken_list = tolken.upper().split()
	title_list = title.upper().split()
	number_elements_tolken = len(tolken_list)
	combinations_list = combinations(title_list, number_elements_tolken)
	count = 0
	for comb in combinations_list:
		title_to_compare = " ".join(comb)
		try:
			distance = Levenshtein.distance(str(tolken.upper()),str(title_to_compare))
		except:
			continue
		if  distance <= 2:
			count = count + 1
	return count 
    	

def get_title_thread(url, search_tolken, c):
			soup = BeautifulSoup(urllib2.urlopen(url))
			title = (soup.title.string).upper()
			print "title: %s" % (title.upper())
			count = check_tolken(search_tolken.upper(), title)
			for i in range(0, count):
				c.increment()


if __name__ == "__main__":
	search_tolken = sys.argv[1].upper()
	g = pygoogle(search_tolken)
	g.pages = 5
	print '*Found %s results*'%(g.get_result_count())
	url_list =  g.get_urls()



	queueLock = threading.Lock()
	workQueue = Queue.Queue(10)
	threads = []

    
	counter = Counter()

	# Create new threads
	for url in url_list:
	    thread = myThread(url, search_tolken, counter)
	    thread.start()
	    threads.append(thread)

	# Wait for queue to empty
	while not workQueue.empty():
	    pass

	# Notify threads it's time to exit
	exitFlag = 1

	# Wait for all threads to complete
	for t in threads:
	    t.join()
	print "Exiting Main Thread"


	print "incidencias: %s em %s urls analisadas" % (counter.value, len(url_list))