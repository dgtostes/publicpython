import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

class IsActiveUser(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'IsActiveUser', 'result': %s}"
        auth_key = self.get_argument('auth_key', 47)
        if auth_key != 47:
            self.write(dic_result_string % '{"nome":"valor", "sobre":"valor 2"}')
        else:        
            self.write(dic_result_string % auth_key)
        #TODO 
        #check if is a active user and write 
        #{'api_class': 'IsActiveUser', 'result': RESULT}
    
class HaveAsk(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'HaveAsk', 'result': %s}"
        auth_key = self.get_argument('auth_key', 47)
        if auth_key != 47:
            self.write(dic_result_string % '{"nome":"valor", "sobre":"valor 2"}')
        else:
            self.write(dic_result_string % auth_key)        
        #TODO 
        #check if user have questions to ansawer 
        #{'api_class': 'HaveAsk', 'result': RESULT}

class GetAsk(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'GetAsk', 'result': %s}"
        auth_key = self.get_argument('auth_key', 47)
        if auth_key != 47:
            self.write(dic_result_string % '{"nome":"valor", "sobre":"valor 2"}')
        else:
            self.write(dic_result_string % auth_key)
        #TODO 
        #check if user data and a dictionary with the RESULT
        #result will be other dictionary where keys will 
        #be tags and value will be asks
        #{'api_class': 'GetAsk', 'result': RESULT}


class AvailableLanguages(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'AvailableLanguages', 'result': %s}"
        self.write(dic_result_string % {1:"POR"})
        #TODO 
        #query on db for all the avaible languages
        #{1: "lang1", 2: "lang2", N: "langN}
        #{'api_class': 'AvailableLanguages', 'result': RESULT}


class AvailableMail(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        email = self.get_argument('email', 47)
        if email != 47:
            self.write(dic_result_string % '{"available": True}')
        else:
            self.write(dic_result_string % email)
        #TODO 
        #query to know if exist email on db
        #{'api_class': 'GetAsk', 'result': RESULT} 

class AvailableCountry(tornado.web.RequestHandler):
    def get(self):
        dic_result_string =  "{'api_class': 'AvailableCountry', 'result': %s}"
        self.write(dic_result_string % {1:"BRA", 2: "USA", 3:"FRA"})
        #TODO 
        #query on db for all the avaible countries
        #{1: "lang1", 2: "lang2", N: "langN}
        #{'api_class': 'AvailableCountry', 'result': RESULT}

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/isactiveuser", IsActiveUser),
    (r"/haveask", HaveAsk),
    (r"/getask", GetAsk),
    (r"/availablelanguages", AvailableLanguages),
    (r"/availablemail", AvailableMail),
    (r"/availablecountry", AvailableCountry),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

