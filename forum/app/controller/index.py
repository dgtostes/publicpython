# -*- coding: utf-8 -*- 
from pyramid.response import Response
from model.model import *
from pyramid.view import view_config

@view_config(route_name='index', renderer='./templates/index.html')
def index(request):
    result = select_topic_by_datetime()
    return {"result_list":"RESULT"}
