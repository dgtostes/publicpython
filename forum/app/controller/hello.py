from pyramid.response import Response
from pyramid.view import view_config
from pyramid.response import Response

@view_config(renderer="hello.pt")
def hello_world(request):
    return Response('Hello')
