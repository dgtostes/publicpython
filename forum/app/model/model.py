# -*- coding: utf-8 -*-
from pymongo import Connection
import datetime

#create a conection
connection = Connection('localhost', 27017)

#create a database
db = connection['forum-database']


def insert_topic(title, desc):
    '''
    recieve a  title and a decription
    and isert a topic info
    '''
    topic = db.topic
    topic_info = {"title": title,
         "description": desc,
         "created": datetime.datetime.now(),
         "_deleted": 0
         }
    topic.insert(topic_info)

def insert_post(title, topic_title, text):
    '''
    recieve a  title, topic_title, texte
    and isert a post info
    '''
    post = db.post
    post_info = {"title": title,
                 "topic_tilte": topic_title,
                 "text": text,
                 "created": datetime.datetime.now(),
                 "_deleted": 0
                 }
    post.insert(post_info)


def select_topic_by_datetime():
    '''
    return a list with all topics
    sorted by last datetime insertion
    '''
    #TODO
    #the same method with "db.topic.find().sort({ created: -1 })"
    date = []
    result = []
    for i in db.topic.find():
        date.append(i["created"])
    
    for i in sorted(date):
        for a in db.topic.find():
            if a["created"] == i:
                result.append(a)
            else:
                pass

    return list(reversed(result))


def select_post_by_datetime():
    '''
    return a list with all posts
    sorted by last datetime insertion
    '''
    #TODO
    #the same method with "db.post.find().sort({ created: -1 })"
    date = []
    result = []
    for i in db.post.find():
        date.append(i["created"])
    
    for i in sorted(date):
        for a in db.post.find():
            if a["created"] == i:
                result.append(a)
            else:
                pass

    return list(reversed(result))


if __name__ == "__main__":
    insert_post("pergunta", "teste", "blabla bla \n\n bla bla")

    for i in select_post_by_datetime():
        print i



