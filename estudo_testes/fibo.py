def fibo(n):
   """
   >>> fibo(1)
   1
   >>> fibo(10)
   55
   """
   if n < 2:
      return n
   else:
      return fibo(n-1) + fibo(n-2)
      
def quadrado(n):
	"""
	>>> quadrado(0)
	0
	>>> quadrado(2)
	4
	>>> quadrado(11)
	121
	"""
	return n*n
 
import doctest
doctest.testmod()



if __name__ == "__main__":
	#print fibo(10)
	pass
